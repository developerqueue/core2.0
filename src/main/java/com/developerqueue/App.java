package com.developerqueue;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        System.out.println(bmi(10, 20));
    }

    public static int bmi(int height, int weight) {
        return weight / (height * height);
    }

}
